import 'package:shared_preferences/shared_preferences.dart';
import 'package:weather_quiz/src/data/entities/model/city_info.dart';
import 'package:weather_quiz/src/utils/constants.dart';

class PrefsHelper {
  Future<SharedPreferences> _getPrefs() async {
    return await SharedPreferences.getInstance();
  }

  Future<bool> clearData() async {
    return (await _getPrefs()).clear();
  }

  void saveCityInfo(CityInfo cityInfo) async {
    final prefs = await _getPrefs();
    prefs.setString(CITY_NAME, cityInfo.name);
    prefs.setInt(CITY_ID, cityInfo.id);
  }

  Future<CityInfo> getCityInfo() async {
    final prefs = await _getPrefs();
    final id = prefs.getInt(CITY_ID) ?? -1;
    final name = prefs.getString(CITY_NAME) ?? '';
    return CityInfo((b) => b..id = id ..name = name);
  }
}