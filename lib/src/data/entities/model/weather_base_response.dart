library weather_base_response;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:weather_quiz/src/data/entities/model/city_info.dart';
import 'package:weather_quiz/src/data/entities/model/weather_forecast.dart';
import 'package:weather_quiz/src/data/entities/serializer/serializers.dart';

part 'weather_base_response.g.dart';

abstract class WeatherBaseResponse
    implements Built<WeatherBaseResponse, WeatherBaseResponseBuilder> {
  @BuiltValueField(wireName: 'cod')
  String get resultCode;

  @BuiltValueField(wireName: 'message')
  int get message;

  @BuiltValueField(wireName: 'cnt')
  int get count;

  @BuiltValueField(wireName: 'city')
  CityInfo get city;

  @BuiltValueField(wireName: 'list')
  BuiltList<WeatherForecast> get forecastList;

  WeatherBaseResponse._();

  factory WeatherBaseResponse([updates(WeatherBaseResponseBuilder b)]) = _$WeatherBaseResponse;

  String toJson() {
    return json
        .encode(serializers.serializeWith(WeatherBaseResponse.serializer, this));
  }

  static WeatherBaseResponse fromJson(String jsonString) {
    return serializers.deserializeWith(
        WeatherBaseResponse.serializer, json.decode(jsonString));
  }

  static Serializer<WeatherBaseResponse> get serializer => _$weatherBaseResponseSerializer;
}
