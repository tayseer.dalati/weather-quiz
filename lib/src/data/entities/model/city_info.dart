library city_info;

import 'dart:convert';

import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:weather_quiz/src/data/entities/serializer/serializers.dart';

part 'city_info.g.dart';

abstract class CityInfo implements Built<CityInfo, CityInfoBuilder> {
  @BuiltValueField(wireName: 'id')
  int get id;

  @BuiltValueField(wireName: 'name')
  String get name;

  CityInfo._();

  factory CityInfo([updates(CityInfoBuilder b)]) = _$CityInfo;

  String toJson() {
    return json
        .encode(serializers.serializeWith(CityInfo.serializer, this));
  }

  static CityInfo fromJson(String jsonString) {
    return serializers.deserializeWith(
        CityInfo.serializer, json.decode(jsonString));
  }

  static Serializer<CityInfo> get serializer => _$cityInfoSerializer;
}
