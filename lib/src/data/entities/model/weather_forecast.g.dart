// GENERATED CODE - DO NOT MODIFY BY HAND

part of weather_forecast;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WeatherForecast> _$weatherForecastSerializer =
    new _$WeatherForecastSerializer();
Serializer<ForecastDetails> _$forecastDetailsSerializer =
    new _$ForecastDetailsSerializer();
Serializer<Weather> _$weatherSerializer = new _$WeatherSerializer();
Serializer<Wind> _$windSerializer = new _$WindSerializer();

class _$WeatherForecastSerializer
    implements StructuredSerializer<WeatherForecast> {
  @override
  final Iterable<Type> types = const [WeatherForecast, _$WeatherForecast];
  @override
  final String wireName = 'WeatherForecast';

  @override
  Iterable<Object> serialize(Serializers serializers, WeatherForecast object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'dt_txt',
      serializers.serialize(object.date, specifiedType: const FullType(String)),
      'main',
      serializers.serialize(object.details,
          specifiedType: const FullType(ForecastDetails)),
      'weather',
      serializers.serialize(object.weather,
          specifiedType:
              const FullType(BuiltList, const [const FullType(Weather)])),
      'wind',
      serializers.serialize(object.wind, specifiedType: const FullType(Wind)),
    ];

    return result;
  }

  @override
  WeatherForecast deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherForecastBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'dt_txt':
          result.date = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'main':
          result.details.replace(serializers.deserialize(value,
                  specifiedType: const FullType(ForecastDetails))
              as ForecastDetails);
          break;
        case 'weather':
          result.weather.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(Weather)]))
              as BuiltList<dynamic>);
          break;
        case 'wind':
          result.wind.replace(serializers.deserialize(value,
              specifiedType: const FullType(Wind)) as Wind);
          break;
      }
    }

    return result.build();
  }
}

class _$ForecastDetailsSerializer
    implements StructuredSerializer<ForecastDetails> {
  @override
  final Iterable<Type> types = const [ForecastDetails, _$ForecastDetails];
  @override
  final String wireName = 'ForecastDetails';

  @override
  Iterable<Object> serialize(Serializers serializers, ForecastDetails object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'temp',
      serializers.serialize(object.temp, specifiedType: const FullType(double)),
      'feels_like',
      serializers.serialize(object.feelsLike,
          specifiedType: const FullType(double)),
      'temp_min',
      serializers.serialize(object.tempMin,
          specifiedType: const FullType(double)),
      'temp_max',
      serializers.serialize(object.tempMax,
          specifiedType: const FullType(double)),
      'pressure',
      serializers.serialize(object.pressure,
          specifiedType: const FullType(double)),
      'humidity',
      serializers.serialize(object.humidity,
          specifiedType: const FullType(double)),
    ];

    return result;
  }

  @override
  ForecastDetails deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new ForecastDetailsBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'temp':
          result.temp = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'feels_like':
          result.feelsLike = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'temp_min':
          result.tempMin = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'temp_max':
          result.tempMax = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'pressure':
          result.pressure = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'humidity':
          result.humidity = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherSerializer implements StructuredSerializer<Weather> {
  @override
  final Iterable<Type> types = const [Weather, _$Weather];
  @override
  final String wireName = 'Weather';

  @override
  Iterable<Object> serialize(Serializers serializers, Weather object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'id',
      serializers.serialize(object.id, specifiedType: const FullType(int)),
      'main',
      serializers.serialize(object.main, specifiedType: const FullType(String)),
      'description',
      serializers.serialize(object.description,
          specifiedType: const FullType(String)),
      'icon',
      serializers.serialize(object.icon, specifiedType: const FullType(String)),
    ];

    return result;
  }

  @override
  Weather deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'id':
          result.id = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'main':
          result.main = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'description':
          result.description = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'icon':
          result.icon = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
      }
    }

    return result.build();
  }
}

class _$WindSerializer implements StructuredSerializer<Wind> {
  @override
  final Iterable<Type> types = const [Wind, _$Wind];
  @override
  final String wireName = 'Wind';

  @override
  Iterable<Object> serialize(Serializers serializers, Wind object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'speed',
      serializers.serialize(object.speed,
          specifiedType: const FullType(double)),
      'deg',
      serializers.serialize(object.degree,
          specifiedType: const FullType(double)),
    ];

    return result;
  }

  @override
  Wind deserialize(Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WindBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'speed':
          result.speed = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
        case 'deg':
          result.degree = serializers.deserialize(value,
              specifiedType: const FullType(double)) as double;
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherForecast extends WeatherForecast {
  @override
  final String date;
  @override
  final ForecastDetails details;
  @override
  final BuiltList<Weather> weather;
  @override
  final Wind wind;

  factory _$WeatherForecast([void Function(WeatherForecastBuilder) updates]) =>
      (new WeatherForecastBuilder()..update(updates)).build();

  _$WeatherForecast._({this.date, this.details, this.weather, this.wind})
      : super._() {
    if (date == null) {
      throw new BuiltValueNullFieldError('WeatherForecast', 'date');
    }
    if (details == null) {
      throw new BuiltValueNullFieldError('WeatherForecast', 'details');
    }
    if (weather == null) {
      throw new BuiltValueNullFieldError('WeatherForecast', 'weather');
    }
    if (wind == null) {
      throw new BuiltValueNullFieldError('WeatherForecast', 'wind');
    }
  }

  @override
  WeatherForecast rebuild(void Function(WeatherForecastBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherForecastBuilder toBuilder() =>
      new WeatherForecastBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WeatherForecast &&
        date == other.date &&
        details == other.details &&
        weather == other.weather &&
        wind == other.wind;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, date.hashCode), details.hashCode), weather.hashCode),
        wind.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WeatherForecast')
          ..add('date', date)
          ..add('details', details)
          ..add('weather', weather)
          ..add('wind', wind))
        .toString();
  }
}

class WeatherForecastBuilder
    implements Builder<WeatherForecast, WeatherForecastBuilder> {
  _$WeatherForecast _$v;

  String _date;
  String get date => _$this._date;
  set date(String date) => _$this._date = date;

  ForecastDetailsBuilder _details;
  ForecastDetailsBuilder get details =>
      _$this._details ??= new ForecastDetailsBuilder();
  set details(ForecastDetailsBuilder details) => _$this._details = details;

  ListBuilder<Weather> _weather;
  ListBuilder<Weather> get weather =>
      _$this._weather ??= new ListBuilder<Weather>();
  set weather(ListBuilder<Weather> weather) => _$this._weather = weather;

  WindBuilder _wind;
  WindBuilder get wind => _$this._wind ??= new WindBuilder();
  set wind(WindBuilder wind) => _$this._wind = wind;

  WeatherForecastBuilder();

  WeatherForecastBuilder get _$this {
    if (_$v != null) {
      _date = _$v.date;
      _details = _$v.details?.toBuilder();
      _weather = _$v.weather?.toBuilder();
      _wind = _$v.wind?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WeatherForecast other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WeatherForecast;
  }

  @override
  void update(void Function(WeatherForecastBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$WeatherForecast build() {
    _$WeatherForecast _$result;
    try {
      _$result = _$v ??
          new _$WeatherForecast._(
              date: date,
              details: details.build(),
              weather: weather.build(),
              wind: wind.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'details';
        details.build();
        _$failedField = 'weather';
        weather.build();
        _$failedField = 'wind';
        wind.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'WeatherForecast', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

class _$ForecastDetails extends ForecastDetails {
  @override
  final double temp;
  @override
  final double feelsLike;
  @override
  final double tempMin;
  @override
  final double tempMax;
  @override
  final double pressure;
  @override
  final double humidity;

  factory _$ForecastDetails([void Function(ForecastDetailsBuilder) updates]) =>
      (new ForecastDetailsBuilder()..update(updates)).build();

  _$ForecastDetails._(
      {this.temp,
      this.feelsLike,
      this.tempMin,
      this.tempMax,
      this.pressure,
      this.humidity})
      : super._() {
    if (temp == null) {
      throw new BuiltValueNullFieldError('ForecastDetails', 'temp');
    }
    if (feelsLike == null) {
      throw new BuiltValueNullFieldError('ForecastDetails', 'feelsLike');
    }
    if (tempMin == null) {
      throw new BuiltValueNullFieldError('ForecastDetails', 'tempMin');
    }
    if (tempMax == null) {
      throw new BuiltValueNullFieldError('ForecastDetails', 'tempMax');
    }
    if (pressure == null) {
      throw new BuiltValueNullFieldError('ForecastDetails', 'pressure');
    }
    if (humidity == null) {
      throw new BuiltValueNullFieldError('ForecastDetails', 'humidity');
    }
  }

  @override
  ForecastDetails rebuild(void Function(ForecastDetailsBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  ForecastDetailsBuilder toBuilder() =>
      new ForecastDetailsBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is ForecastDetails &&
        temp == other.temp &&
        feelsLike == other.feelsLike &&
        tempMin == other.tempMin &&
        tempMax == other.tempMax &&
        pressure == other.pressure &&
        humidity == other.humidity;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc(
                $jc($jc($jc(0, temp.hashCode), feelsLike.hashCode),
                    tempMin.hashCode),
                tempMax.hashCode),
            pressure.hashCode),
        humidity.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('ForecastDetails')
          ..add('temp', temp)
          ..add('feelsLike', feelsLike)
          ..add('tempMin', tempMin)
          ..add('tempMax', tempMax)
          ..add('pressure', pressure)
          ..add('humidity', humidity))
        .toString();
  }
}

class ForecastDetailsBuilder
    implements Builder<ForecastDetails, ForecastDetailsBuilder> {
  _$ForecastDetails _$v;

  double _temp;
  double get temp => _$this._temp;
  set temp(double temp) => _$this._temp = temp;

  double _feelsLike;
  double get feelsLike => _$this._feelsLike;
  set feelsLike(double feelsLike) => _$this._feelsLike = feelsLike;

  double _tempMin;
  double get tempMin => _$this._tempMin;
  set tempMin(double tempMin) => _$this._tempMin = tempMin;

  double _tempMax;
  double get tempMax => _$this._tempMax;
  set tempMax(double tempMax) => _$this._tempMax = tempMax;

  double _pressure;
  double get pressure => _$this._pressure;
  set pressure(double pressure) => _$this._pressure = pressure;

  double _humidity;
  double get humidity => _$this._humidity;
  set humidity(double humidity) => _$this._humidity = humidity;

  ForecastDetailsBuilder();

  ForecastDetailsBuilder get _$this {
    if (_$v != null) {
      _temp = _$v.temp;
      _feelsLike = _$v.feelsLike;
      _tempMin = _$v.tempMin;
      _tempMax = _$v.tempMax;
      _pressure = _$v.pressure;
      _humidity = _$v.humidity;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(ForecastDetails other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$ForecastDetails;
  }

  @override
  void update(void Function(ForecastDetailsBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$ForecastDetails build() {
    final _$result = _$v ??
        new _$ForecastDetails._(
            temp: temp,
            feelsLike: feelsLike,
            tempMin: tempMin,
            tempMax: tempMax,
            pressure: pressure,
            humidity: humidity);
    replace(_$result);
    return _$result;
  }
}

class _$Weather extends Weather {
  @override
  final int id;
  @override
  final String main;
  @override
  final String description;
  @override
  final String icon;

  factory _$Weather([void Function(WeatherBuilder) updates]) =>
      (new WeatherBuilder()..update(updates)).build();

  _$Weather._({this.id, this.main, this.description, this.icon}) : super._() {
    if (id == null) {
      throw new BuiltValueNullFieldError('Weather', 'id');
    }
    if (main == null) {
      throw new BuiltValueNullFieldError('Weather', 'main');
    }
    if (description == null) {
      throw new BuiltValueNullFieldError('Weather', 'description');
    }
    if (icon == null) {
      throw new BuiltValueNullFieldError('Weather', 'icon');
    }
  }

  @override
  Weather rebuild(void Function(WeatherBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherBuilder toBuilder() => new WeatherBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Weather &&
        id == other.id &&
        main == other.main &&
        description == other.description &&
        icon == other.icon;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc($jc($jc(0, id.hashCode), main.hashCode), description.hashCode),
        icon.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Weather')
          ..add('id', id)
          ..add('main', main)
          ..add('description', description)
          ..add('icon', icon))
        .toString();
  }
}

class WeatherBuilder implements Builder<Weather, WeatherBuilder> {
  _$Weather _$v;

  int _id;
  int get id => _$this._id;
  set id(int id) => _$this._id = id;

  String _main;
  String get main => _$this._main;
  set main(String main) => _$this._main = main;

  String _description;
  String get description => _$this._description;
  set description(String description) => _$this._description = description;

  String _icon;
  String get icon => _$this._icon;
  set icon(String icon) => _$this._icon = icon;

  WeatherBuilder();

  WeatherBuilder get _$this {
    if (_$v != null) {
      _id = _$v.id;
      _main = _$v.main;
      _description = _$v.description;
      _icon = _$v.icon;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Weather other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Weather;
  }

  @override
  void update(void Function(WeatherBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Weather build() {
    final _$result = _$v ??
        new _$Weather._(
            id: id, main: main, description: description, icon: icon);
    replace(_$result);
    return _$result;
  }
}

class _$Wind extends Wind {
  @override
  final double speed;
  @override
  final double degree;

  factory _$Wind([void Function(WindBuilder) updates]) =>
      (new WindBuilder()..update(updates)).build();

  _$Wind._({this.speed, this.degree}) : super._() {
    if (speed == null) {
      throw new BuiltValueNullFieldError('Wind', 'speed');
    }
    if (degree == null) {
      throw new BuiltValueNullFieldError('Wind', 'degree');
    }
  }

  @override
  Wind rebuild(void Function(WindBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WindBuilder toBuilder() => new WindBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is Wind && speed == other.speed && degree == other.degree;
  }

  @override
  int get hashCode {
    return $jf($jc($jc(0, speed.hashCode), degree.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('Wind')
          ..add('speed', speed)
          ..add('degree', degree))
        .toString();
  }
}

class WindBuilder implements Builder<Wind, WindBuilder> {
  _$Wind _$v;

  double _speed;
  double get speed => _$this._speed;
  set speed(double speed) => _$this._speed = speed;

  double _degree;
  double get degree => _$this._degree;
  set degree(double degree) => _$this._degree = degree;

  WindBuilder();

  WindBuilder get _$this {
    if (_$v != null) {
      _speed = _$v.speed;
      _degree = _$v.degree;
      _$v = null;
    }
    return this;
  }

  @override
  void replace(Wind other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$Wind;
  }

  @override
  void update(void Function(WindBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$Wind build() {
    final _$result = _$v ?? new _$Wind._(speed: speed, degree: degree);
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
