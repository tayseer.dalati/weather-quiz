// GENERATED CODE - DO NOT MODIFY BY HAND

part of weather_base_response;

// **************************************************************************
// BuiltValueGenerator
// **************************************************************************

Serializer<WeatherBaseResponse> _$weatherBaseResponseSerializer =
    new _$WeatherBaseResponseSerializer();

class _$WeatherBaseResponseSerializer
    implements StructuredSerializer<WeatherBaseResponse> {
  @override
  final Iterable<Type> types = const [
    WeatherBaseResponse,
    _$WeatherBaseResponse
  ];
  @override
  final String wireName = 'WeatherBaseResponse';

  @override
  Iterable<Object> serialize(
      Serializers serializers, WeatherBaseResponse object,
      {FullType specifiedType = FullType.unspecified}) {
    final result = <Object>[
      'cod',
      serializers.serialize(object.resultCode,
          specifiedType: const FullType(String)),
      'message',
      serializers.serialize(object.message, specifiedType: const FullType(int)),
      'cnt',
      serializers.serialize(object.count, specifiedType: const FullType(int)),
      'city',
      serializers.serialize(object.city,
          specifiedType: const FullType(CityInfo)),
      'list',
      serializers.serialize(object.forecastList,
          specifiedType: const FullType(
              BuiltList, const [const FullType(WeatherForecast)])),
    ];

    return result;
  }

  @override
  WeatherBaseResponse deserialize(
      Serializers serializers, Iterable<Object> serialized,
      {FullType specifiedType = FullType.unspecified}) {
    final result = new WeatherBaseResponseBuilder();

    final iterator = serialized.iterator;
    while (iterator.moveNext()) {
      final key = iterator.current as String;
      iterator.moveNext();
      final dynamic value = iterator.current;
      switch (key) {
        case 'cod':
          result.resultCode = serializers.deserialize(value,
              specifiedType: const FullType(String)) as String;
          break;
        case 'message':
          result.message = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'cnt':
          result.count = serializers.deserialize(value,
              specifiedType: const FullType(int)) as int;
          break;
        case 'city':
          result.city.replace(serializers.deserialize(value,
              specifiedType: const FullType(CityInfo)) as CityInfo);
          break;
        case 'list':
          result.forecastList.replace(serializers.deserialize(value,
                  specifiedType: const FullType(
                      BuiltList, const [const FullType(WeatherForecast)]))
              as BuiltList<dynamic>);
          break;
      }
    }

    return result.build();
  }
}

class _$WeatherBaseResponse extends WeatherBaseResponse {
  @override
  final String resultCode;
  @override
  final int message;
  @override
  final int count;
  @override
  final CityInfo city;
  @override
  final BuiltList<WeatherForecast> forecastList;

  factory _$WeatherBaseResponse(
          [void Function(WeatherBaseResponseBuilder) updates]) =>
      (new WeatherBaseResponseBuilder()..update(updates)).build();

  _$WeatherBaseResponse._(
      {this.resultCode, this.message, this.count, this.city, this.forecastList})
      : super._() {
    if (resultCode == null) {
      throw new BuiltValueNullFieldError('WeatherBaseResponse', 'resultCode');
    }
    if (message == null) {
      throw new BuiltValueNullFieldError('WeatherBaseResponse', 'message');
    }
    if (count == null) {
      throw new BuiltValueNullFieldError('WeatherBaseResponse', 'count');
    }
    if (city == null) {
      throw new BuiltValueNullFieldError('WeatherBaseResponse', 'city');
    }
    if (forecastList == null) {
      throw new BuiltValueNullFieldError('WeatherBaseResponse', 'forecastList');
    }
  }

  @override
  WeatherBaseResponse rebuild(
          void Function(WeatherBaseResponseBuilder) updates) =>
      (toBuilder()..update(updates)).build();

  @override
  WeatherBaseResponseBuilder toBuilder() =>
      new WeatherBaseResponseBuilder()..replace(this);

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is WeatherBaseResponse &&
        resultCode == other.resultCode &&
        message == other.message &&
        count == other.count &&
        city == other.city &&
        forecastList == other.forecastList;
  }

  @override
  int get hashCode {
    return $jf($jc(
        $jc(
            $jc($jc($jc(0, resultCode.hashCode), message.hashCode),
                count.hashCode),
            city.hashCode),
        forecastList.hashCode));
  }

  @override
  String toString() {
    return (newBuiltValueToStringHelper('WeatherBaseResponse')
          ..add('resultCode', resultCode)
          ..add('message', message)
          ..add('count', count)
          ..add('city', city)
          ..add('forecastList', forecastList))
        .toString();
  }
}

class WeatherBaseResponseBuilder
    implements Builder<WeatherBaseResponse, WeatherBaseResponseBuilder> {
  _$WeatherBaseResponse _$v;

  String _resultCode;
  String get resultCode => _$this._resultCode;
  set resultCode(String resultCode) => _$this._resultCode = resultCode;

  int _message;
  int get message => _$this._message;
  set message(int message) => _$this._message = message;

  int _count;
  int get count => _$this._count;
  set count(int count) => _$this._count = count;

  CityInfoBuilder _city;
  CityInfoBuilder get city => _$this._city ??= new CityInfoBuilder();
  set city(CityInfoBuilder city) => _$this._city = city;

  ListBuilder<WeatherForecast> _forecastList;
  ListBuilder<WeatherForecast> get forecastList =>
      _$this._forecastList ??= new ListBuilder<WeatherForecast>();
  set forecastList(ListBuilder<WeatherForecast> forecastList) =>
      _$this._forecastList = forecastList;

  WeatherBaseResponseBuilder();

  WeatherBaseResponseBuilder get _$this {
    if (_$v != null) {
      _resultCode = _$v.resultCode;
      _message = _$v.message;
      _count = _$v.count;
      _city = _$v.city?.toBuilder();
      _forecastList = _$v.forecastList?.toBuilder();
      _$v = null;
    }
    return this;
  }

  @override
  void replace(WeatherBaseResponse other) {
    if (other == null) {
      throw new ArgumentError.notNull('other');
    }
    _$v = other as _$WeatherBaseResponse;
  }

  @override
  void update(void Function(WeatherBaseResponseBuilder) updates) {
    if (updates != null) updates(this);
  }

  @override
  _$WeatherBaseResponse build() {
    _$WeatherBaseResponse _$result;
    try {
      _$result = _$v ??
          new _$WeatherBaseResponse._(
              resultCode: resultCode,
              message: message,
              count: count,
              city: city.build(),
              forecastList: forecastList.build());
    } catch (_) {
      String _$failedField;
      try {
        _$failedField = 'city';
        city.build();
        _$failedField = 'forecastList';
        forecastList.build();
      } catch (e) {
        throw new BuiltValueNestedFieldError(
            'WeatherBaseResponse', _$failedField, e.toString());
      }
      rethrow;
    }
    replace(_$result);
    return _$result;
  }
}

// ignore_for_file: always_put_control_body_on_new_line,always_specify_types,annotate_overrides,avoid_annotating_with_dynamic,avoid_as,avoid_catches_without_on_clauses,avoid_returning_this,lines_longer_than_80_chars,omit_local_variable_types,prefer_expression_function_bodies,sort_constructors_first,test_types_in_equals,unnecessary_const,unnecessary_new
