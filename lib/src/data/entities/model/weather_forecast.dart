library weather_forecast;

import 'dart:convert';

import 'package:built_collection/built_collection.dart';
import 'package:built_value/built_value.dart';
import 'package:built_value/serializer.dart';
import 'package:weather_quiz/src/data/entities/serializer/serializers.dart';

part 'weather_forecast.g.dart';

abstract class WeatherForecast
    implements Built<WeatherForecast, WeatherForecastBuilder> {
  @BuiltValueField(wireName: 'dt_txt')
  String get date;
  @BuiltValueField(wireName: 'main')
  ForecastDetails get details;
  @BuiltValueField(wireName: 'weather')
  BuiltList<Weather> get weather;
  @BuiltValueField(wireName: 'wind')
  Wind get wind;

  WeatherForecast._();

  factory WeatherForecast([updates(WeatherForecastBuilder b)]) = _$WeatherForecast;

  String toJson() {
    return json
        .encode(serializers.serializeWith(WeatherForecast.serializer, this));
  }

  static WeatherForecast fromJson(String jsonString) {
    return serializers.deserializeWith(
        WeatherForecast.serializer, json.decode(jsonString));
  }

  static Serializer<WeatherForecast> get serializer => _$weatherForecastSerializer;
}

abstract class ForecastDetails
    implements Built<ForecastDetails, ForecastDetailsBuilder> {
  @BuiltValueField(wireName: 'temp')
  double get temp;
  @BuiltValueField(wireName: 'feels_like')
  double get feelsLike;
  @BuiltValueField(wireName: 'temp_min')
  double get tempMin;
  @BuiltValueField(wireName: 'temp_max')
  double get tempMax;
  @BuiltValueField(wireName: 'pressure')
  double get pressure;
  @BuiltValueField(wireName: 'humidity')
  double get humidity;

  ForecastDetails._();

  factory ForecastDetails([updates(ForecastDetailsBuilder b)]) = _$ForecastDetails;

  String toJson() {
    return json
        .encode(serializers.serializeWith(ForecastDetails.serializer, this));
  }

  static ForecastDetails fromJson(String jsonString) {
    return serializers.deserializeWith(
        ForecastDetails.serializer, json.decode(jsonString));
  }

  static Serializer<ForecastDetails> get serializer => _$forecastDetailsSerializer;
}

abstract class Weather
    implements Built<Weather, WeatherBuilder> {
  @BuiltValueField(wireName: 'id')
  int get id;
  @BuiltValueField(wireName: 'main')
  String get main;
  @BuiltValueField(wireName: 'description')
  String get description;
  @BuiltValueField(wireName: 'icon')
  String get icon;

  Weather._();

  factory Weather([updates(WeatherBuilder b)]) = _$Weather;

  String toJson() {
    return json
        .encode(serializers.serializeWith(Weather.serializer, this));
  }

  static Weather fromJson(String jsonString) {
    return serializers.deserializeWith(
        Weather.serializer, json.decode(jsonString));
  }

  static Serializer<Weather> get serializer => _$weatherSerializer;
}

abstract class Wind
    implements Built<Wind, WindBuilder> {
  @BuiltValueField(wireName: 'speed')
  double get speed;
  @BuiltValueField(wireName: 'deg')
  double get degree;

  Wind._();

  factory Wind([updates(WindBuilder b)]) = _$Wind;

  String toJson() {
    return json
        .encode(serializers.serializeWith(Wind.serializer, this));
  }

  static Wind fromJson(String jsonString) {
    return serializers.deserializeWith(
        Wind.serializer, json.decode(jsonString));
  }

  static Serializer<Wind> get serializer => _$windSerializer;
}
