library serializers;

import 'package:built_collection/built_collection.dart';
import 'package:built_value/serializer.dart';
import 'package:built_value/standard_json_plugin.dart';
import 'package:weather_quiz/src/data/entities/model/city_info.dart';
import 'package:weather_quiz/src/data/entities/model/weather_base_response.dart';
import 'package:weather_quiz/src/data/entities/model/weather_forecast.dart';

part 'serializers.g.dart';

@SerializersFor(const [
  WeatherBaseResponse,
  WeatherForecast,
  ForecastDetails,
  CityInfo,
  Weather,
  Wind,
  Null,
])
final Serializers serializers = (_$serializers.toBuilder()
  ..addPlugin(StandardJsonPlugin()))
    .build();
