import 'package:weather_quiz/src/data/db_helper.dart';
import 'package:weather_quiz/src/data/entities/model/city_info.dart';
import 'package:weather_quiz/src/data/entities/model/weather_base_response.dart';
import 'package:weather_quiz/src/data/entities/model/weather_forecast.dart';
import 'package:weather_quiz/src/data/http_helper.dart';
import 'package:weather_quiz/src/data/prefs_helper.dart';
import 'package:built_collection/built_collection.dart';

class Repository {
  final HttpHelper _httpHelper;
  final PrefsHelper _prefsHelper;
  final DatabaseHelper _databaseHelper;

  Repository(this._httpHelper, this._prefsHelper, this._databaseHelper);

  /// Shared Preferences ///

  void saveCityInfo(CityInfo cityInfo) => _prefsHelper.saveCityInfo(cityInfo);

  Future<CityInfo> getCityInfo() async => await _prefsHelper.getCityInfo();



  /// Database ///

  Future insertForecastList(BuiltList<WeatherForecast> forecastList) async =>
      await _databaseHelper.insertForecastList(forecastList);

  Future<List<WeatherForecast>> getForecastList() async =>
      await _databaseHelper.getForecastList();



  /// HTTP ///

  Future<WeatherBaseResponse> getWeatherForecast() async =>
      await _httpHelper.getWeatherForecast();
}
