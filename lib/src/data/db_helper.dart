import 'package:sqflite/sqflite.dart';
import 'package:built_collection/built_collection.dart';
import 'package:weather_quiz/src/data/entities/model/weather_forecast.dart';

class DatabaseHelper {
  Database database;

  DatabaseHelper() {
    _openDB();
  }

  Future _openDB() async {
    database = await openDatabase('weather_quiz', version: 1,
        onCreate: (Database db, int version) async {
      String createForecastTableQuery = '''CREATE TABLE Forecast(
            date TEXT PRIMARY KEY, 
            details TEXT,
            weather TEXT,
            wind TEXT
          )''';
      await db.execute(createForecastTableQuery);
    }).then((result) {
      print('DatabaseHelper => openDB => RESULT = $result');
    }).catchError((error) {
      print('DatabaseHelper => openDB => ERROR = $error');
    });

  }

  Future insertForecastList(BuiltList<WeatherForecast> forecastList) async {
    if (database == null || (database == null && !database.isOpen))
      database = await openDatabase('weather_quiz');

    String query = '''INSERT INTO 
      Forecast(date, details, weather, wind) VALUES (?, ?, ?, ?)''';

    await database.transaction<void>((txn) async {
      await txn.rawDelete('DELETE FROM Forecast');
      for (WeatherForecast forecast in forecastList)
        await txn.rawInsert(query, [
          forecast.date,
          forecast.details.toJson(),
          forecast.weather.first.toJson(),
          forecast.wind.toJson(),
        ]);
    });
  }

  Future<List<WeatherForecast>> getForecastList() async {
    if (database == null || (database == null && !database.isOpen))
      database = await openDatabase('weather_quiz');

    List<WeatherForecast> forecastList = List();
    List<Map<String, dynamic>> list = await database.rawQuery('SELECT * FROM Forecast');
    for (Map<String, dynamic> item in list)
      forecastList.add(WeatherForecast((b) => b
          ..date = item['date']
          ..wind.replace(Wind.fromJson(item['wind']))
          ..details.replace(ForecastDetails.fromJson(item['details']))
          ..weather.replace([Weather.fromJson(item['weather'])])
      ));
    return forecastList;
  }
}
