import 'package:dio/dio.dart';
import 'dart:async';

import 'package:weather_quiz/src/data/entities/model/weather_base_response.dart';
import 'package:weather_quiz/src/utils/constants.dart';

class HttpHelper {
  final Dio dio;

  HttpHelper(this.dio) {
    dio.interceptors.add(LogInterceptor(
//      requestBody: true,
//      responseBody: true,
        ));
  }

  Future<WeatherBaseResponse> getWeatherForecast() async {
    try {
      final apiUrl =
          'http://api.openweathermap.org/data/2.5/forecast?id=292223&appid=83a22d6fc616b9a94fc3e1ca4441881d&units=metric&lang=en';
      final response = await dio.get(apiUrl);

      if (response.statusCode == RESPONSE_OK && response.data != null) {
        final result = WeatherBaseResponse.fromJson(response.data);
        return result.resultCode == '200' ? result : throw new Exception();
      } else throw new Exception();
    } catch (e) {
      print('ERROR = $e');
      return Future.error('Error getting weather forecast.');
    }
  }
}
