import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:kiwi/kiwi.dart' as kiwi;
import 'package:weather_quiz/src/data/entities/model/weather_forecast.dart';
import 'package:weather_quiz/src/utils/colors.dart';
import 'package:weather_quiz/src/utils/route_generator.dart';

import 'bloc.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _bloc = kiwi.Container().resolve<HomeBloc>();
  Size screenSize;

  @override
  void dispose() {
    _bloc.close();
    super.dispose();
  }

  @override
  void initState() {
    SystemChrome.setSystemUIOverlayStyle(
        SystemUiOverlayStyle(statusBarColor: Colors.transparent));
    super.initState();
    _bloc.add(GetWeatherForecast());
  }

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      body: BlocBuilder(
        bloc: _bloc,
        builder: (context, HomeState state) {
          if (state is ForecastLoaded)
            return Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(bottom: 16.0, top: 48.0, left: 16.0),
                  child: Text(
                    'Weather in ${_bloc.cityInfo.name}',
                    style: TextStyle(
                      fontSize: 28,
                      color: DARK_BLUE,
                      fontFamily: 'JosefinSans',
                      fontWeight: FontWeight.w900,
                    ),
                  ),
                ),
                Expanded(
                  child: Flex(
                    direction: Axis.vertical,
                    children: <Widget>[
                      Flexible(
                          flex: 1,
                          child: weatherDayTile(
                              'Today', 'cvr_snow.jpg', DateTime.now())),
                      Flexible(
                          flex: 1,
                          child: weatherDayTile('Tomorrow', 'cvr_clear.jpg',
                              DateTime.now().add(Duration(days: 1)))),
                      Flexible(
                          flex: 1,
                          child: weatherDayTile(
                              'After Tomorrow',
                              'cvr_partly_cloudy.jpg',
                              DateTime.now().add(Duration(days: 2)))),
                    ],
                  ),
                ),
                SizedBox(height: 16.0),
              ],
            );
          else if (state is ErrorLoadingForecast)
            return Center(
              child: InkWell(
                onTap: () => _bloc.add(GetWeatherForecast()),
                child: Padding(
                  padding: EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Text(
                        'Failed to load Weather Forecast!\nTap to refresh.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 18,
                          color: DARK_GREY,
                          fontFamily: 'JosefinSans',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                      SizedBox(height: 8.0),
                      Icon(
                        Icons.refresh,
                        size: 32,
                        color: DARK_GREY,
                      ),
                    ],
                  ),
                ),
              ),
            );
          else if (state is Loading)
            return Center(
              child: SpinKitThreeBounce(
                color: VERY_LIGHT_GREY,
                size: 80,
              ),
            );
          else
            return Container();
        },
      ),
    );
  }

  Widget weatherDayTile(String title, String cover, DateTime dateTime) {
    final WeatherForecast forecast = _bloc.getNoonForecast(dateTime.day);
    if (forecast != null)
      return InkWell(
        onTap: () {
          Navigator.pushNamed(context, RouteNames.DAILY_FORECAST, arguments: {
            'city_name': _bloc.cityInfo.name,
            'forecast': forecast,
          });
        },
        child: Container(
          width: MediaQuery.of(context).size.width,
          margin: EdgeInsets.symmetric(horizontal: 16, vertical: 8.0),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(12)),
            boxShadow: [
              BoxShadow(color: Colors.grey[300], spreadRadius: 2, blurRadius: 4)
            ],
          ),
          child: Column(
            children: <Widget>[
              Expanded(
                child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(12)),
                  child: Stack(
                    children: <Widget>[
                      Image.asset(
                        'assets/images/$cover',
                        width: MediaQuery.of(context).size.width,
                        fit: BoxFit.cover,
                      ),
                      Container(
                        color: Color.fromARGB(30, 0, 0, 0),
                      ),
                      Align(
                        alignment: Alignment.bottomLeft,
                        child: Padding(
                          padding: const EdgeInsets.symmetric(
                              horizontal: 16.0, vertical: 16.0),
                          child: Text(
                            title,
                            style: TextStyle(
                              fontFamily: 'JosefinSans',
                              fontWeight: FontWeight.w900,
                              color: WHITE,
                              fontSize: 21,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 16, vertical: 20.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      'Humidity ${forecast.details.humidity.toInt()}%',
                      style: TextStyle(
                        fontFamily: 'JosefinSans',
                        fontWeight: FontWeight.w700,
                        color: DARK_GREY,
                        fontSize: 16,
                      ),
                    ),
                    Text(
                      '${_bloc.getMinTemp(dateTime.day)}/${_bloc.getMaxTemp(dateTime.day)}°',
                      style: TextStyle(
                        fontFamily: 'JosefinSans',
                        fontWeight: FontWeight.w900,
                        color: DARK_GREY,
                        fontSize: 21,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    else
      return Container();
  }
}
