import 'package:flutter/material.dart';
import 'package:weather_quiz/src/data/entities/model/weather_forecast.dart';
import 'package:weather_quiz/src/utils/colors.dart';

class DailyForecast extends StatefulWidget {
  final WeatherForecast forecast;
  final String cityName;

  DailyForecast(this.forecast, this.cityName);

  @override
  _DailyForecastState createState() => _DailyForecastState();
}

class _DailyForecastState extends State<DailyForecast> {
  Size screenSize;

  @override
  Widget build(BuildContext context) {
    screenSize = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: getBackgroundColor(widget.forecast.weather.first.main),
      body: Container(
        width: screenSize.width,
        height: screenSize.height,
        decoration: BoxDecoration(
            image: DecorationImage(
          alignment: screenSize.width > screenSize.height
              ? Alignment.topRight
              : Alignment.center,
          image: AssetImage(
              'assets/images/${getConditionImage(widget.forecast.weather.first.main)}'),
        )),
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(vertical: 24.0),
            child: Column(
              mainAxisSize: MainAxisSize.max,
              crossAxisAlignment: CrossAxisAlignment.start,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: EdgeInsets.only(right: 16, left: 24.0),
                    child: RichText(
                      text: TextSpan(
                          children: widget.forecast.weather.first.description
                              .split(' ')
                              .map((word) {
                        return TextSpan(
                          text: word + '\n',
                          style: TextStyle(
                            fontSize: 52,
                            color: Colors.white,
                            fontFamily: 'JosefinSans',
                            fontWeight: FontWeight.w500,
                          ),
                        );
                      }).toList()),
                    ),
                  ),
                ),
                Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: EdgeInsets.only(right: 16, left: 24.0),
                      child: Text(
                        '${widget.cityName}',
                        style: TextStyle(
                          fontSize: 46,
                          color: Colors.white,
                          fontFamily: 'JosefinSans',
                          fontWeight: FontWeight.w700,
                        ),
                      ),
                    ),
                    SingleChildScrollView(
                      scrollDirection: Axis.horizontal,
                      child: Row(
                        children: <Widget>[
                          Container(
                            padding: EdgeInsets.only(right: 16, left: 24.0),
                            child: Text(
                              '${widget.forecast.details.temp.toInt()}°',
                              style: TextStyle(
                                  fontSize: 96,
                                  color: Colors.white,
                                fontFamily: 'JosefinSans',
                                fontWeight: FontWeight.w700,
                              ),
                            ),
                          ),
                          detailView('Wind',
                              '${widget.forecast.wind.speed.toInt()}km/h'),
                          detailView('Humidity',
                              '${widget.forecast.details.humidity.toInt()}%'),
                          detailView('Pressure',
                              '${widget.forecast.details.pressure.toInt()}MB'),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget detailView(String title, String value) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 12.0, vertical: 8.0),
      margin: EdgeInsets.symmetric(horizontal: 6.0),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
          color: Colors.black12,
          spreadRadius: 2,
          blurRadius: 2,
        )
      ]),
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                fontSize: 13,
                color: GREY,
                fontFamily: 'JosefinSans',
                fontWeight: FontWeight.w700,
              ),
            ),
            SizedBox(height: 4.0),
            Text(
              value,
              style: TextStyle(
                fontSize: 18,
                color: DARK_BLUE,
                fontFamily: 'JosefinSans',
                fontWeight: FontWeight.w700,
              ),
            ),
          ],
        ),
      ),
    );
  }

  String getConditionImage(String condition) {
    switch (condition) {
      case 'Clear':
        return 'img_clear.png';
      case 'Clouds':
        return 'img_partly_cloudy.png';
      case 'Rain':
        return 'img_rain.png';
      case 'Thunderstorm':
        return 'img_thunderstorm.png';
      case 'Drizzle':
        return 'img_light_rain.png';
      default:
        return 'img_clear.png';
    }
  }

  Color getBackgroundColor(String condition) {
    switch (condition) {
      case 'Clear':
        return Color.fromARGB(255, 171, 222, 229); // light blue
      case 'Clouds':
        return Color.fromARGB(255, 156, 230, 205); // green
      case 'Rain':
        return Color.fromARGB(255, 243, 183, 147); // orange
      case 'Thunderstorm':
        return Color.fromARGB(255, 126, 92, 246); // purple
      case 'Drizzle':
        return Color.fromARGB(255, 30, 90, 178); // blue
      case 'Snow':
        return Color.fromARGB(255, 248, 175, 225); // pink
      default:
        return Color.fromARGB(255, 171, 222, 229); // light blue
    }
  }
}
