import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:weather_quiz/src/data/entities/model/city_info.dart';
import 'package:weather_quiz/src/data/entities/model/weather_base_response.dart';
import 'package:weather_quiz/src/data/entities/model/weather_forecast.dart';
import 'package:weather_quiz/src/data/repository.dart';
import 'package:weather_quiz/src/srceens/home/bloc.dart';
import './bloc.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  final Repository _repository;
  final List<WeatherForecast> forecastList = List();
  CityInfo cityInfo;

  HomeBloc(this._repository);

  @override
  HomeState get initialState => InitialHomeState();

  @override
  Stream<HomeState> mapEventToState(HomeEvent event) async* {
    if (event is GetWeatherForecast) yield* mapGetWeatherForecast(event);
  }

  Stream<HomeState> mapGetWeatherForecast(GetWeatherForecast event) async* {
    if (!(await needToRefresh())) {
      yield ForecastLoaded();
      return;
    }
    yield Loading();
    WeatherBaseResponse weatherResult;
    String errorMessage = '';
    await _repository
        .getWeatherForecast()
        .then((result) => weatherResult = result)
        .catchError((error) {
      errorMessage = error;
    });

    if (errorMessage.isEmpty && weatherResult != null) {
      forecastList.clear();
      forecastList.addAll(weatherResult.forecastList);
      cityInfo = weatherResult.city;
      yield ForecastLoaded();
      _repository.saveCityInfo(weatherResult.city);
      _repository.insertForecastList(weatherResult.forecastList);
    } else
      yield ErrorLoadingForecast();
  }

  /// check if the forecast of the current day is stored in database
  /// return [false] when it's stored and [true] when it's not.
  Future<bool> needToRefresh() async {
    final cityInfo = await _repository.getCityInfo();
    if (cityInfo.name.isNotEmpty) {
      this.cityInfo = cityInfo;
      final list = await _repository.getForecastList();
      forecastList.addAll(list);
      for (WeatherForecast forecast in list)
        if (DateTime.parse(forecast.date).day == DateTime.now().day)
          return false;
    }
    return true;
  }

  /// get maximum temperature in a specific day
  int getMaxTemp(int day) {
    int max = 0;
    for (WeatherForecast forecast in getWeatherByDay(day)) {
      if (forecast.details.temp > max) max = forecast.details.temp.toInt();
    }
    return max;
  }

  /// get minimum temperature in a specific day
  int getMinTemp(int day) {
    int min = 100;
    for (WeatherForecast forecast in getWeatherByDay(day)) {
      if (forecast.details.temp < min) min = forecast.details.temp.toInt();
    }
    return min;
  }

  /// get all forecast of a specific day
  List<WeatherForecast> getWeatherByDay(int day) {
    return forecastList
        .where((forecast) => DateTime.parse(forecast.date).day == day)
        .toList();
  }

  /// get noon forecast of a specific day
  WeatherForecast getNoonForecast(int day) {
    for (WeatherForecast forecast in getWeatherByDay(day)) {
      if (DateTime.parse(forecast.date).hour >= 12) return forecast;
    }
    return null;
  }
}
