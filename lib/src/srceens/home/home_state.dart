import 'package:meta/meta.dart';

@immutable
abstract class HomeState {}

class InitialHomeState extends HomeState {}

class Loading extends HomeState {}

class ForecastLoaded extends HomeState {}

class ErrorLoadingForecast extends HomeState {}
