const BASE_URL = 'http://12.34.56.789:8000/api';


// User information
const CITY_NAME = 'city_name';
const CITY_ID = 'city_id';

const APPLICATION_LANGUAGE = 'application_language';

// Status codes
const RESPONSE_OK = 200;
const RESPONSE_CREATED = 204;
const RESPONSE_UNAUTHORIZED = 401;
const RESPONSE_INTERNAL_ERROR = 500;

// Exception types
const NO_ERROR = 0;
const NETWORK_EXCEPTION = 1;
const ERROR_GETTING_DATA = 2;
const DESERIALIZE_EXCEPTION = 3;
const INVALID_INPUT = 4;