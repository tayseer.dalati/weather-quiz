import 'package:dio/dio.dart';
import 'package:weather_quiz/src/data/db_helper.dart';
import 'package:weather_quiz/src/data/http_helper.dart';
import 'package:weather_quiz/src/data/prefs_helper.dart';
import 'package:weather_quiz/src/data/repository.dart';
import 'package:weather_quiz/src/srceens/home/bloc.dart';
import 'package:kiwi/kiwi.dart' as Kiwi;

void iniKiwi() {
  Kiwi.Container()
    ..registerInstance(Dio(
      BaseOptions(
        connectTimeout: 20000,
        responseType: ResponseType.plain,
        headers: {'Accept': 'application/json'},
      ),
    ))
    ..registerSingleton((c) => PrefsHelper())
    ..registerSingleton((c) => DatabaseHelper())
    ..registerSingleton((c) => HttpHelper(c.resolve()))
    ..registerSingleton((c) => Repository(c.resolve(), c.resolve(), c.resolve()))
    ..registerFactory((c) => HomeBloc(c.resolve()));
}
