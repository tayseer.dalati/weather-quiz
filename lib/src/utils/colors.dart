import 'package:flutter/material.dart';

const YELLOW = Color.fromARGB(255, 255, 204, 2);
const LIGHT_YELLOW = Color.fromARGB(255, 246, 236, 200);
const BANANA_YELLOW = Color.fromARGB(255, 247, 197, 68);
const SPLASH_COLOR = Color.fromARGB(80, 0, 0, 0);


const WHITE = Color.fromARGB(255, 255, 255, 255);
const PURPLE = Color.fromARGB(255, 152, 92, 247);
const LIGHT_RED = Color.fromARGB(255, 242, 175, 175);
const BLUE = Color.fromARGB(255, 0, 89, 175);
const HIGHLIGHT_BLUE = Color.fromARGB(40, 0, 89, 175);
const DARK_BLUE = Color(0xFF000067);
//const DARK_BLUE = Color.fromARGB(255, 0, 62, 171);
const ORANGE = Color.fromARGB(255, 235, 168, 68);
const GREY = Color.fromARGB(255, 150, 150, 150);
const LIGHT_GREY = Color.fromARGB(255, 216, 216, 216);
const VERY_LIGHT_GREY = Color.fromARGB(255, 236, 236, 236);
const DARK_GREY = Color.fromARGB(255, 75, 75, 75);
const HINT_COLOR = Color.fromARGB(255, 210, 210, 210);
const OFF_WHITE = Color.fromARGB(255, 245, 246, 247);
