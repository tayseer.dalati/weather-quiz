import 'package:weather_quiz/src/utils/colors.dart';
import 'package:flutter/material.dart';

showSnackBarMessage(
    GlobalKey<ScaffoldState> _scaffoldKey,
    String message, {
      Color backgroundColor = DARK_GREY,
      Duration duration = const Duration(milliseconds: 3000),
    }) {
  _scaffoldKey.currentState.showSnackBar(
    new SnackBar(
      duration: duration,
      content: Text(
        message,
        style: TextStyle(
          fontSize: 16,
          color: Colors.white,
          fontWeight: FontWeight.w600,
        ),
      ),
      backgroundColor: backgroundColor,
    ),
  );
}

showMessageDialog(BuildContext context, String message) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text(message),
        actions: [
          FlatButton(
            child: Text("Ok"),
            onPressed: () {},
          ),
        ],
      );
    },
  );
}

showConfirmDialog(BuildContext context, String message, Function onConfirm) {
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return AlertDialog(
        content: Text(
          message,
          style: TextStyle(
            color: DARK_GREY,
            fontSize: 16,
          ),
        ),
        actions: [
          FlatButton(
            child: Text(
              "Cancel",
              style: TextStyle(
                color: GREY,
                fontWeight: FontWeight.w600,
                fontSize: 14,
              ),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          FlatButton(
            child: Text(
              "Ok",
              style: TextStyle(
                color: DARK_BLUE,
                fontWeight: FontWeight.w600,
                fontSize: 14,
              ),
            ),
            onPressed: () {
              onConfirm();
              Navigator.pop(context);
            },
          ),
        ],
      );
    },
  );
}