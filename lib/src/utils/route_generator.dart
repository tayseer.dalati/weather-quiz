import 'package:weather_quiz/src/srceens/home/daily_forecast.dart';
import 'package:weather_quiz/src/srceens/home/home.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class RouteGenerator {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    final args = settings.arguments;

    switch (settings.name) {
      case RouteNames.HOME_SCREEN:
        return MaterialPageRoute(
          builder: (_) => Home(),
        );
      case RouteNames.DAILY_FORECAST:
        return CupertinoPageRoute(
          builder: (_) => DailyForecast(
              (args as Map)['forecast'],
              (args as Map)['city_name'],
          ),
        );
      default:
        return _errorRoute();
    }
  }

  static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }
}

class RouteNames {
  static const HOME_SCREEN = '/';
  static const DAILY_FORECAST = '/daily_forecast';
}
