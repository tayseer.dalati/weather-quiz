import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:weather_quiz/src/utils/colors.dart';
import 'package:weather_quiz/src/utils/no_splash_factory.dart';
import 'package:weather_quiz/src/utils/route_generator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class App extends StatefulWidget {

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      locale: Locale('en'),
      debugShowCheckedModeBanner: false,
      theme: Theme.of(context).copyWith(
        splashFactory: NoSplashFactory(),
        primaryColor: BLUE,
        cursorColor: DARK_BLUE,
        cupertinoOverrideTheme: CupertinoThemeData(
          primaryColor: DARK_BLUE,
        ),
      ),
      supportedLocales: [
        Locale('en'),
      ],
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      onGenerateRoute: RouteGenerator.generateRoute,
      initialRoute: RouteNames.HOME_SCREEN,
    );
  }
}